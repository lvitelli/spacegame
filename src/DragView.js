import ui.ImageView as ImageView;
import math.geom.Rect as Rect;
import math.geom.intersect as intersect;

exports = Class(ImageView,function(supr){
    
    
    this.init = function (opts){
        
        supr(this,"init",[opts]);
        
        var obj = this;
        var startPoint = null;
        
        var _onStartFunction = function (ev,point){
            if (intersect.pointAndRect(point,obj._getRect())) {
                obj.emit("DragStart");
                startPoint = point;
                startPoint.x -= obj.style.x;
                startPoint.y -= obj.style.y;
            }else{
                startPoint = null;
            }
        }
        
        var _onMoveFunction = function (ev,point){
            if (startPoint != null) {
                obj.emit("Drag");
                obj.style.update({
                    x: point.x - startPoint.x,
                    y: point.y - startPoint.y
                })
            }
        }
        
        var _onOutFunction = function (ev,point){
            if (startPoint != null) {
                obj.emit("DragStop");
            }
        }
        
        this._makeDraggable = function (){
            if (!this._draggable) {
                this._draggable = true;
                this._opts.superview.on("InputStart",_onStartFunction);
                this._opts.superview.on("InputMove",_onMoveFunction);
                this._opts.superview.on("InputOut",_onOutFunction);
            }
        }
        
        this._notDraggable = function (){
            if (this._draggable) {
                this._draggable = false;
                this._opts.superview.removeListener("InputStart",_onStartFunction);
                this._opts.superview.removeListener("InputMove",_onMoveFunction);
                this._opts.superview.removeListener("InputOut",_onOutFunction);
            }
        }
        
        if (opts.draggable) {
            this._makeDraggable();
        }
    }
    
    this.setDraggable = function (bool){
        if (bool) {
            this._makeDraggable();
        }else{
            this._notDraggable();
        }
    }
    
    this.isDraggable = function (){
        return this._draggable;
    }
    
    this._getRect = function (){
        return new Rect(this.style.x,this.style.y,this.style.width,this.style.height);
    }
    
    this.removeFromParentView = function (){
        this._notDraggable();
        this._opts.superview.removeSubview(this);
    }
});