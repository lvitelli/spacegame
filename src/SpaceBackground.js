import ui.View as View;
import ui.ParticleEngine as ParticleEngine;
import device;
import animate;

exports = Class (View, function (supr){
    
    this.init = function (){
        
        var opts = {
            superview: GC.app.view,
            height: device.height,
            width: device.width,
            x:0, y:0,
            backgroundColor: 'black'
        }
        
        supr(this,"init",[opts]);
        
        this.particle = new ParticleEngine({
            parent: this
        });
        
        var density = 0.5
        GC.app.engine.on("Tick",bind(this,function(dt){
            if (Math.random() < density) {
                this._emit();
            }
            this.particle.runTick(dt);
        }));
    }
    
    this._emit = function (){
        
        var data = this.particle.obtainParticleArray(5);
        for (var i= 0; i<5; i++) {
            var ran = Math.random();
            if (ran < 0.02) {
                var pObj = data[i];
                pObj.image = "resources/images/star.png";
                pObj.x = device.width;
                pObj.y = Math.random()* device.height;
                pObj.dx = -device.width/8;
                pObj.width = device.height/100;
                pObj.height = device.height/100;
                pObj.ttl = 10000;
            }else if (ran < 0.05) {
                var pObj = data[i];
                pObj.image = "resources/images/star.png";
                pObj.x = device.width;
                pObj.y = Math.random()* device.height;
                pObj.dx = -device.width/7;
                pObj.width = device.height/60;
                pObj.height = device.height/60;
                pObj.ttl = 9000;
            }else if (ran < 0.07) {
                var pObj = data[i];
                pObj.image = "resources/images/star.png";
                pObj.x = device.width;
                pObj.y = Math.random()* device.height;
                pObj.dx = -device.width/6;
                pObj.width = device.height/40;
                pObj.height = device.height/40;
                pObj.ttl = 8000;
            }else if (ran < 0.073) {
                var pObj = data[i];
                pObj.image = "resources/images/meteor.png";
                pObj.x = device.width;
                pObj.y = Math.random()* device.height;
                pObj.dx = -device.width/7;
                pObj.width = device.height/20;
                pObj.height = device.height/20;
                pObj.r = Math.random()*Math.PI*2
                pObj.dr = Math.PI;
                pObj.anchorX = device.height/30;
                pObj.anchorY = device.height/30;
                pObj.ttl = 10000;
            }
        }
        this.particle.emitParticles(data);
    }

    this.pause = function (){
        this.pauseView = new View({
            x: 0, y: 0,
            height: this.style.height,
            width: this.style.width,
            backgroundColor: "black",
            opacity:0,
            superview: this
        });
        animate(this.pauseView).now({opacity:1},300);
    }

    this.resume = function (){
        if(this.pauseView != null)
            animate(this.pauseView).now({opacity:0},300).then(bind(this,"removeSubview",this.pauseView));
    }
});