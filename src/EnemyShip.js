import ui.ImageView as ImageView;
import math.geom.Rect as Rect;
import src.EnemyShot as EnemyShot;
import animate;
import device;

exports = Class (ImageView,function (supr){

    /*param:
     *  type: index of type
     *  points: array of points with delay {x,y,delay}
     *  cb on exit: function
     */
    
    this.init = function (param){
        
        this._points = param.points;
        this._startPoint = this._points.shift();
        this._finishCb = param.cb;
        var opts = {};
        if (param.type == 0) {
            opts = {
                height: device.height/12,
                width: device.width/12,
                superview: GC.app.view,
                x: this._startPoint.x,
                y: this._startPoint.y,
                anchorX: device.width/24,
                anchorY: device.height/24,
                image: "resources/images/enemy/"+param.type+".png"
            }
            this.SHOT_DENSITY = 1.5;
            this.HEALTH = 20;
            
        }
        
        this._points[this._points.length-1].x = -opts.width;
        supr(this,"init",[opts]);
        this.type = param.type;
        
        this._currentShots = new Array();
        GC.app.enemyShips.push(this);
        
        setTimeout(bind(this,function (){
            this.startShoot();
            //this._mainAnimation = animate(this).now({x: 0 - device.width/12, y: finishY},7000,animate.linear).then(function(){
            var firstPoint = this._points.shift();
            this._mainAnimation = animate(this).now({x: firstPoint.x, y: firstPoint.y},firstPoint.delay,animate.linear);
            for (var i in this._points) {
                this._mainAnimation = this._mainAnimation.then({x: this._points[i].x,y: this._points[i].y},this._points[i].delay, animate.linear);
            }
            this._mainAnimation = this._mainAnimation.then(function(){
                this._opts.superview.removeSubview(this);
                var index = GC.app.enemyShips.indexOf(this);
                this.stopShoot();
                GC.app.enemyShips.splice(index, 1);
                this._finishCb();
            });
        }),500);
    }
    
    this.getRect = function (){
        return new Rect(this.style.x, this.style.y, this.style.width, this.style.height);
    }
    
    this.damage = function (damage){
        this.HEALTH -= damage;
        if (this.HEALTH < 1) {
            this.destroy();
        }
    }
    
    this.destroy = function (){
        this.stopShoot();
        this._finishCb();
        this._mainAnimation = animate(this).now({scale: 0.4, opacity:0, r: Math.PI*2, x: this.style.x - 200},2000).then(function (){
            this._opts.superview.removeSubview(this);
            var index = GC.app.enemyShips.indexOf(this);
            GC.app.enemyShips.splice(index, 1);
        });
    }
    
    this.pause = function (){
        if (!this._isInPause) {
            this._isInPause = true;
            if(this._mainAnimation)this._mainAnimation.pause();
            if (this._shotInterval != null)
                clearInterval(this._shotInterval);
                
            for (var i in this._currentShots) {
                this._currentShots[i].pause();
            }
        }
    }
    
    this.resume = function (){
        if (this._isInPause) {
            this._mainAnimation.resume();
            this._isInPause = false;
            for (var i in this._currentShots) {
                this._currentShots[i].resume();
            }
            if (this._isInShoot) {
                this.startShoot();
            }
        }
    }
    
    this.startShoot = function (){
        this._isInShoot = true;
        this._shotInterval = setInterval(bind(this,"shoot"),1000/this.SHOT_DENSITY);
    }
    
    this.stopShoot = function (){
        this._isInShoot = false;
        if (this._shotInterval != null) {
            clearInterval(this._shotInterval);
        }
        this._shotInterval = null
    }
    
    this.shoot = function (){
        new EnemyShot(this);
    }
});