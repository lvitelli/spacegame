import ui.ImageView as ImageView;
import src.PlayerShot as PlayerShot;
import event.input.drag as drag;
import ui.ParticleEngine as ParticleEngine;
import src.DragView as DragView
import math.geom.Rect as Rect;
import device;
import animate;

exports = Class(DragView,function (supr){
    this.init = function (){
        
        var opts = {
            height: device.height/6,
            width: device.height/6,
            x: -device.height/6,
            y: device.height/2 - device.height/12,
            superview: GC.app.view,
            anchorX: device.width/12,
            anchorY: device.height/12,
            zIndex: 5,
            image: "resources/images/spaceShip.png"  ,
            draggable: true
        }
        supr(this,"init",[opts]);
        
        this.HEALTH = 100;
        this.RESISTANCE = 5; 
        this.SHOT_DENSITY = 4; //number of strokes for second
        this.SHOT_TYPE = 1;
        
        this._emitView = this;
        
        
        this._isInPause = false;
        this._currentShots = new Array();
        this._startPoint = {
            x: 50,
            y: device.height/2 - device.height/12
        }
        
        this.on("DragStop",function (){
            animate(this).now({x: 0, y:this._startPoint.y},700,animate.easeOut).then(this._startPoint,300,animate.easeInOut); 
        });
        
        this.particle = new ParticleEngine({
            parent: GC.app.view
        });
        
        var density = 0.5
        GC.app.engine.on("Tick",bind(GC.app,bind(this,function(dt){
            if (Math.random() < density) {
                this._emitSmokeOrFlame();
            }
            this.particle.runTick(dt);
        })));
        
    }
    
    this.initInTheGame = function (cb){
        this.setDraggable(true);
        GC.app.healthBar.setValue(100);
        this.HEALTH = 100;
        animate(this).now({x: 300, y:(device.height-this.style.height)/2},1000).
        then({x: 50},1000).then(cb);
    }
    
    this.startShoot = function (){
        this._isInShoot = true;
        this._shotInterval = setInterval(bind(this,"shoot"),1000/this.SHOT_DENSITY);
    }
    
    this.stopShoot = function (){
        this._isInShoot = false;
        if (this._shotInterval != null) {
            clearInterval(this._shotInterval);
        }
        this._shotInterval = null
    }
    
    this.shoot = function (){
        var c = new PlayerShot(this);
    }
    
    this.pause = function (){
        if (!this._isInPause) {
            this.setDraggable(false);
            this._isInPause = true;
            if (this._shotInterval != null)
                clearInterval(this._shotInterval);
                
            for (var i in this._currentShots) {
                this._currentShots[i].pause();
            }
        }
    }
    
    this.resume = function (){
        if (this._isInPause) {
            this.setDraggable(true);
            this._isInPause = false;
            for (var i in this._currentShots) {
                this._currentShots[i].resume();
            }
            if (this._isInShoot) {
                this.startShoot();
            }
        }
    }
    
    this._emitSmokeOrFlame = function (){
        
        var data = this.particle.obtainParticleArray(5);
        for (var i= 0; i<5; i++) {
            
            var ran = Math.random();
            var pObj = data[i];
            pObj.r = 0;
            pObj.dr = Math.PI;
            pObj.dx = -300;
            pObj.dopacity = -1;
            pObj.dy = Math.random()*500 - 250;
            
            if (this.HEALTH > 40 && this.HEALTH <= 60 && ran < 0.1) {
                pObj.image = "resources/images/smoke.png";
                pObj.x = this._emitView.style.x + this.style.width/8;
                pObj.y = this._emitView.style.y + this.style.height/3;
                pObj.anchorX = this.style.width/6;
                pObj.anchorY = this.style.height/6;
                pObj.width = this.style.width/3;
                pObj.height = this.style.height/3;
                pObj.ttl = 4000;
            } else if (this.HEALTH > 0 && this.HEALTH <= 40 && ran < 0.2) {
                pObj.image = "resources/images/smoke.png";
                pObj.x = this._emitView.style.x + this.style.width/8;
                pObj.y = this._emitView.style.y + this.style.height/4;
                pObj.anchorX = this.style.width/6;
                pObj.anchorY = this.style.height/6;
                pObj.width = this.style.width/2;
                pObj.height = this.style.height/2;
                pObj.ttl = 4000;
            }else if (this.HEALTH <= 5 && ran < 0.4) {
                pObj.image = "resources/images/smoke.png";
                if (Math.random()> 0.5) {
                    pObj.image = "resources/images/flame.png";
                }
                pObj.x = this._emitView.style.x + this.style.width/8;
                pObj.y = this._emitView.style.y + this.style.height/4;
                pObj.anchorX = this.style.width/6;
                pObj.anchorY = this.style.height/6;
                pObj.width = this.style.width/2;
                pObj.height = this.style.height/2;
                pObj.ttl = 4000;
            }
        }
        this.particle.emitParticles(data);
    }
    
    this.setHealth = function (newValue){
        if (newValue <= 0) {
            newValue = 0;
            this.finishLevelAnimation();
        }
        this.HEALTH = newValue;
        GC.app.healthBar.setValue(newValue);
    }
    
    this.reduceHealthOf = function(red){
        var correctRed = red*this.RESISTANCE;
        this.setHealth(this.HEALTH-correctRed);
        GC.app.healthBar.reduceValueOf(correctRed);
    }
    
    this.die = function (){
        this.stopShoot();
        this.setDraggable(false);
        animate(this).now({
            x:device.width /2 - this.style.width/2,
            y: device.height/2 - this.style.height/2,
            scale: 1.2
        },2000).then(function (){
        }).wait(500).then({x: -this.style.width, opacity: 0.2, scale: 0.4, r: Math.PI*4},2000).then(bind(this,function (){    
            this.HEALTH = 100;
        }));
    }
    
    this.finishLevelAnimation = function (){
        this.stopShoot();
        this.setDraggable(false);
        animate(this).now({
            x: device.width /2 - this.style.width/2,
            y: device.height/2 - this.style.height/2,
            scale: 1.2
        },2000).then(function (){
        }).wait(500).then({x: device.width+100},1000,animate.easeIn).then(bind(this,function (){  
            this.HEALTH = 100;
        }));
    }
    this.getRect = function (){
        return new Rect(this.style.x, this.style.y, this.style.width, this.style.height);
    }
});