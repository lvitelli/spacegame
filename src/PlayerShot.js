import ui.ImageView as ImageView;
import math.geom.Point as Point;
import math.geom.intersect as intersect;
import device;
import animate;

exports = Class (ImageView,function(supr){
    this.init = function (craft,r){
        
        var opts = {
            height: 20,
            width: 40,
            superview: GC.app.view,
            zIndex: 4,
            x: craft.style.x + craft.style.width/2 - 20,
            y: craft.style.y + craft.style.height/2 - 10,
            image: "resources/images/playerShot.png",
        }
        if(r != null) console.log("ciao");
        
        if (craft.SHOT_TYPE == 0) {
            this.DAMAGE = 5;
        }else if (craft.SHOT_TYPE == 1){
            this.DAMAGE = 4;
            if(r == null){
                new exports(craft,-1);
                new exports(craft,1);
            }else{
                opts.r = Math.PI/4*r;
            }
        }
        
        supr(this,"init",[opts]);
        
        craft._currentShots.push(this);
        var finishBind = bind(this,function (){
            GC.app.view.removeSubview(this);
            var index = craft._currentShots.indexOf(this);
            craft._currentShots.splice(index, 1);
        });
        if(r != null){
            this._mainAnimation = animate(this).now({
                x: this.style.x + device.width,
                y: opts.y + (this.style.x + device.width)/2*r
            },1500,animate.linear).then(finishBind);
        }else{
            this._mainAnimation = animate(this).now({
                x: this.style.x + device.width
            },1500,animate.linear).then(finishBind);
        }


        this.tick = bind(this, function (dt){
            var en = GC.app.enemyShips;
            for (var i in en) {
                if (intersect.pointAndRect(this.getPoint(),en[i].getRect()) && en[i].HEALTH > 0) {
                    en[i].damage(this.DAMAGE);
                    this.destroy();
                }
            }
        });
    }
    
    this.pause = function (){
        this._mainAnimation.pause();
    }
    
    this.resume = function (){
        this._mainAnimation.resume();
    }
    
    this.getPoint = function (){
        return new Point (this.style.x + this.style.width, this.style.y + this.style.height/2);
    }
    
    this.destroy = function (){
        this._mainAnimation.clear();
        var smoke = new ImageView({
            height: this.style.width,
            width: this.style.width,
            x: this.style.x,
            y: this.style.y - (this.style.width-this.style.height)/2,
            superview: GC.app.view,
            zIndex: this.style.zIndex,
            image: "resources/images/smoke.png",
            anchorX: this.style.width/2,
            anchorY: this.style.width/2
        });
        this._opts.superview.removeSubview(this);
        this._mainAnimation = animate(smoke).now({r: Math.PI*1.5, opacity: 0, scale: 2},1000).then(bind(GC.app.view,"removeSubview",smoke));
    }
    
});
