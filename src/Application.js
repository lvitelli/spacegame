import src.SpaceBackground as SpaceBackground;
import src.PlayerSpacecraft as PlayerSpacecraft;
import ui.ImageView as ImageView;
import src.EnemyShip as EnemyShip;
import ui.View as View;
import src.HealthBar as HealthBar;
import src.DragView as DragView;
import device;

exports = Class(GC.Application, function () {

	this.initUI = function () {
		this._spaceBack = new SpaceBackground();
		this.playerSpaceCraft = new PlayerSpacecraft();
		this.healthBar = new HealthBar();
		this.enemyShips = new Array();
		
		this.IS_IN_PAUSE = false;
		this.pauseButton = new View({
			height: 100,
			width: 100,
			x: device.width -120,
			y: 20,
			superview: GC.app.view,
			zIndex: 20,
			backgroundColor: "red",
			draggable : true
		});
		this.pauseButton.on("InputSelect",function (){
			if (GC.app.IS_IN_PAUSE) {
				GC.app.resume();
			}else{
				GC.app.pause();
			}
		});

	};
	
	this.launchUI = function () {
		this.playerSpaceCraft.initInTheGame(bind(this.playerSpaceCraft,"startShoot"));
		
		this.generate = function () {
			this.generateEnemy({
				type: 0,
				points: [{
					x: device.width,
					y: 300
				},{
					x: device.width*3/4,
					y: device.height - 300,
					delay: 600
				},{
					x: device.width/4,
					y: 300,
					delay: 1200
				},{
					x: 0,
					y: device.height - 300,
					delay: 600
				}],
				cb: bind(this,"generate")
			});
		}
		this.generate();
	};
	
	this.pause = function (){
		this.IS_IN_PAUSE = true;
		this.playerSpaceCraft.pause();
		for (var i in this.enemyShips) {
			this.enemyShips[i].pause();
		}
		this._spaceBack.pause();
	}
	
	this.resume = function (){
		this.IS_IN_PAUSE = false;
		this.playerSpaceCraft.resume();
		for (var i in this.enemyShips) {
			this.enemyShips[i].resume();
		}
		this._spaceBack.resume();
	}
	
	this.blockPlayerInteration = function (){
		if (this._blockView == null) {
			this._blockView = new View({
				superview: this.view,
				height: device.height,
				width: device.width,
				x:0, y:0,
				zIndex: 20
			});
		}
	}
	
	this.unblockPlayerInteration = function (){
		if (this._blockView != null) {
			this.view.removeSubview(this._blockView);
			this._blockView = null;
		}
	}
	
	this.generateEnemy = function (params){
		new EnemyShip(params);
	}
});
