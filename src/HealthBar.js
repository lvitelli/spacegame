import ui.View as View;
import animate;
import device;

exports = Class (View, function (supr){
    
    var _value = 100;
    
    this.init = function (){
        
        var opts = {
            superview: GC.app.view,
            zIndex: 10,
            height: device.height/40,
            width: device.width/3 - device.height/40,
            x: device.height/40,
            y: device.height/40
        }
        
        supr(this,"init",[opts]);
        
        this._bar = new View({
            superview: this,
            x: 0, y: 0,
            height: opts.height,
            width: opts.width,
            backgroundColor: "#15F200"
        });
    }
    
    this.getValue = function (){
        return _value;
    }
    
    this.setValue = function (newValue){
        if (newValue < 0) {
            newValue = 0;
        }
        _value = newValue;
        var newColor = "#FF0C00";
        if (newValue > 60) {
            newColor = "#15F200";
        }else if (newValue > 30) {
            newColor = "#FFEB00";
        }
        animate(this._bar).now({width: (device.width/3 - device.height/40)*newValue/100}).
            then(bind(this._bar.style,"update",{backgroundColor: newColor}));
    }
    
    this.reduceValueOf = function (red){
        if (red > _value) {
            red = _value;
        }
        this.setValue(_value-red);
    }
    
    
});