import ui.ImageView as ImageView;
import math.geom.Point as Point;
import math.geom.intersect as intersect;
import device;
import animate;

exports = Class (ImageView,function(supr){
    this.init = function (craft){
        
        var opts = {};
        if (craft.type == 0) {
            opts = {
                height: 20,
                width: 40,
                superview: GC.app.view,
                zIndex: 4,
                x: craft.style.x + craft.style.width/2 - 20,
                y: craft.style.y + craft.style.height/2 - 10,
                image: "resources/images/playerShot.png",
                r: Math.PI
            }
            this.DAMAGE = 3;
        }
        
        supr(this,"init",[opts]);
        
        craft._currentShots.push(this);
        this._mainAnimation = animate(this).now({x: this.style.x - device.width},3000,animate.linear).then(bind(this,function (){
            GC.app.view.removeSubview(this);
            var index = craft._currentShots.indexOf(this);
            craft._currentShots.splice(index, 1);
        }));
        
        this.tick = bind(this, function (dt){
            var player = GC.app.playerSpaceCraft;
            if (intersect.pointAndRect(this.getPoint(),player.getRect()) && player.HEALTH > 0) {
                player.reduceHealthOf(this.DAMAGE);
                this.destroy();
            }
        });
    }
    
    this.pause = function (){
        this._mainAnimation.pause();
    }
    
    this.resume = function (){
        this._mainAnimation.resume();
    }
    
    this.getPoint = function (){
        return new Point (this.style.x + this.style.width, this.style.y + this.style.height/2);
    }
    
    this.destroy = function (){
        this._mainAnimation.clear();
        var smoke = new ImageView({
            height: this.style.width,
            width: this.style.width,
            x: this.style.x,
            y: this.style.y - (this.style.width-this.style.height)/2,
            superview: GC.app.view,
            zIndex: this.style.zIndex,
            image: "resources/images/smoke.png",
            anchorX: this.style.width/2,
            anchorY: this.style.width/2
        });
        this._opts.superview.removeSubview(this);
        this._mainAnimation = animate(smoke).now({r: Math.PI*1.5, opacity: 0, scale: 2},1000).then(bind(GC.app.view,"removeSubview",smoke));
    }
    
});